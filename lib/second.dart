import 'dart:async';

import 'package:flutter/material.dart';
import 'package:hello_world/teste.dart';
import 'package:hello_world/third.dart';

class SecondPage extends StatelessWidget {
  // Criação de tela com navegacão para voltar, caso queira que seja outra "Activity" ou "ViewController", criar root

  String title;
  Future<List<Teste>> testezinho;

  SecondPage(@required this.title, @required this.testezinho); // Construtor

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        new Expanded(
          child: new Scaffold(
            appBar: AppBar(
              // Criação de toolbar
              title: Text(this.title),
              backgroundColor: Colors.red,
            ),
            body: Center(
              child: FutureBuilder<List<Teste>>(
                future: testezinho,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      // Criação de Lista
                        itemCount: snapshot.data.length,
                        // Definição de quantidade de itens na lista
                        itemBuilder: (context, index) {
                          // Criação de "For" para lista aonde é passado o contexto e posição
                          final item = snapshot.data[index];
                          final count = index + 1;
                          return new ListTile(
                            onTap: () {
                              var alert = new AlertDialog(
                                // Criação do dialog
                                content: new Text(item.title), // Texto do Botão
                              );
                              showDialog(
                                  context: context,
                                  child: alert);
                            },
                            leading: new Material(
                              shape: new CircleBorder(),
                              child: Image.network(
                                item.thumbnailUrl,
                                width: 50.0,
                                height: 50.0,
                              ),
                            ),
                            title: new Text(
                                item.title + '$count'), // Titulo do item
                            subtitle: new Text(
                                item.title + '$count'), // Subtitulo do item
                          );
                        });
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }

                  // By default, show a loading spinner
                  return CircularProgressIndicator();
                },
              ),
            ),
          ),
        ),
        new Container(
            height: 50.0,
            width: double.infinity,
            color: Colors.white,
            child: new RaisedButton(
                child: Text('Abrir terceira tela'),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context) => ThirdApp(testezinho)));
                })
        )
      ],
    );
  }
}
