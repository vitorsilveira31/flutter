import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hello_world/second.dart';
import 'package:hello_world/teste.dart';
import 'package:http/http.dart' as http;

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      theme: new ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: new MyHomePageState(),
    );
  }
}

class MyHomePageState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(body: createSafeArea(context));
  }

  TextEditingController login = TextEditingController();
  TextEditingController senha = TextEditingController();

  SafeArea createSafeArea(BuildContext context) {
    return new SafeArea(child: createColumn(context, MainAxisAlignment.center));
  }

  Column createColumn(BuildContext context, MainAxisAlignment alignment) {
    return new Column(
      mainAxisAlignment: alignment,
      children: <Widget>[
        createContainerWith(widget: createImage(), left: 25.0, right: 25.0),
        createContainerWith(
            widget: createLoginTextFormField(), left: 25.0, right: 25.0),
        createContainerWith(
            widget: createPasswordTextFormField(), left: 25.0, right: 25.0),
        createContainerWith(
            widget: createLoginButton(context),
            top: 25.0,
            alignment: Alignment.center),
      ],
    );
  }

  Image createImage() {
    return new Image(
      image: AssetImage(
          'assets/lake.jpg'), // Path do local da imagem - Para exibir corretamente colocar o path também no arquivo pubspec.yaml
    );
  }

  Container createContainerWith(
      {@required Widget widget,
      double left = 0.0,
      double right = 0.0,
      double top = 0.0,
      double bottom = 0.0,
      Alignment alignment}) {
    return new Container(
        alignment: alignment,
        margin:
            EdgeInsets.only(left: left, right: right, top: top, bottom: bottom),
        child: widget);
  }

  TextFormField createLoginTextFormField() {
    return new TextFormField(
        controller: login,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
            hintStyle: TextStyle(color: Colors.purple), hintText: 'Login'),
        style: TextStyle(color: Colors.cyan));
  }

  TextFormField createPasswordTextFormField() {
    return new TextFormField(
        controller: senha,
        keyboardType: TextInputType.text,
        obscureText: true,
        decoration: InputDecoration(
            hintStyle: TextStyle(color: Colors.purple), hintText: 'Password'),
        style: TextStyle(color: Colors.cyan));
  }

  RaisedButton createLoginButton(BuildContext context) {
    return new RaisedButton(
        child: Text('Logar'),
        color: Colors.blue,
        onPressed: () {
          showDialog(
              context: context, builder: (_) => createAlertDialog(context));
        });
  }

  AlertDialog createAlertDialog(BuildContext context) {
    return new AlertDialog(
        content: new Text(!login.text.isEmpty
            ? senha.text.isEmpty
                ? login.text + " você é top"
                : login.text + " você é top e a sua senha é " + senha.text
            : 'Logado'),
        actions: <Widget>[createOkButton(context)]);
  }

  RaisedButton createOkButton(BuildContext context) {
    return new RaisedButton(
      child: Text('Ok'),
      color: Colors.blueAccent,
      onPressed: () {
        Navigator.pop(context);
        createTimerWithAction(context);
      },
    );
  }

  Timer createTimerWithAction(BuildContext context) {
    return new Timer(const Duration(seconds: 1), () {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (_) =>
                  SecondPage('Topperson', fetchPhotos(http.Client()))));
    });
  }

  List<Teste> parsePhotos(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<Teste>((json) => Teste.fromJson(json)).toList();
  }

  Future<List<Teste>> fetchPhotos(http.Client client) async {
    final response =
        await client.get('https://jsonplaceholder.typicode.com/photos');

    return parsePhotos(response.body);
  }
}
