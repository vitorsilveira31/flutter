class Teste {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  Teste({this.albumId, this.id, this.title, this.url, this.thumbnailUrl});

  factory Teste.fromJson(Map<String, dynamic> json) {
    return Teste(
      albumId: json['albumId'],
      id: json['id'],
      title: json['title'],
      url: json['url'],
      thumbnailUrl: json['thumbnailUrl'],
    );
  }
}