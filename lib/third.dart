import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hello_world/teste.dart';


class ThirdApp extends StatelessWidget {

  Future<List<Teste>> testezinho;

  ThirdApp(@required this.testezinho);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      // Definição do tipo de Root
      theme: new ThemeData(
        primarySwatch: Colors.teal, // Especificação da cor do root
      ),
      home: new ThirdPageState(testezinho), // Inserção do "layout"
    );
  }
}

class ThirdPageState extends StatelessWidget {

  Future<List<Teste>> testezinho;

  ThirdPageState(@required this.testezinho);

  @override
  Widget build(BuildContext context) {
    return new Container(
      height: 250.0,
      width: double.infinity,
      color: Colors.white,
      child: FutureBuilder<List<Teste>>(
          future: testezinho,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return GridView.builder(

                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                ),
                itemCount: snapshot.data.length,
                itemBuilder: (context, index) {
                  return GridTile(
                    child: Image.network(snapshot.data[index].thumbnailUrl),

                  );
                },
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner
            return LinearProgressIndicator();
          }),
    );
  }
}
